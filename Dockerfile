FROM node:latest
COPY ./ /home/node/sips
WORKDIR /home/node/sips
RUN yarn install && yarn build
USER node
CMD yarn start
