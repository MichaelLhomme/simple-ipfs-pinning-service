import { Router } from 'express'

import { PinsService } from '~/resources/pins/pins.service'
import { Pin, PinStatus, PinFilter, PinsResult } from '~~/types/pins'

import {
  BadRequestException,
  NotYetImplementedException
} from '~/utils/exceptions'


const PinsController = Router()
const service = new PinsService()


// Helper for pin filtering
const filterPin = (filter: PinFilter, pinStatus: PinStatus) => {
  // Filter by CID
  if(filter.cid && pinStatus.pin.cid !== filter.cid) {
    return false
  }

  // Filter by status
  if(filter.status) {
    const statuses = filter.status.split(',')
    if(!statuses.includes(pinStatus.status)) {
      return false
    }
  }

  // Filter by name
  if(filter.name) {
    if(filter.match) {
      switch(filter.match) {
      case 'exact':
        if(pinStatus.pin.name !== filter.name) {
          return false
        }
        break

      case 'iexact':
        if(pinStatus.pin.name.toUpperCase() !== filter.name.toUpperCase()) {
          return false
        }
        break

      case 'partial':
        if(!pinStatus.pin.name.includes(filter.name)) {
          return false
        }
        break

      case 'ipartial':
        if(!pinStatus.pin.name.match(new RegExp(filter.name, 'i'))) {
          return false
        }
        break

      default: throw new Error(`Unknown match '${filter.match}'`)
      }
    } else if(pinStatus.pin.name !== filter.name) {
      return false
    }
  }

  /*
   * Filter by date
   * TODO use Date instead of string comparison ?
   */
  if(filter.before && filter.before.localeCompare(pinStatus.created) >= 0) {
    return false
  }
  if(filter.after && filter.after.localeCompare(pinStatus.created) <= 0) {
    return false
  }

  // Filter by metadata
  if(filter.meta) {
    const remaining =
      Object
        .entries(JSON.parse(filter.meta))
        .filter(([ key, value ]) => value !== pinStatus.pin.meta?.[key])

    if(remaining.length > 0) {
      return false
    }
  }

  return true
}

// Helper to sort pins by descending date
const comparePin = (a: PinStatus, b: PinStatus) => {
  return -1 * a.created.localeCompare(b.created)
}


// List pins
PinsController.get('/', (req, res, next) => {
  const filter: PinFilter = req.query as PinFilter
  const limit: number = filter.limit ? filter.limit : 10

  service.list(req.user)
    .then(pins => {
    // Filters and sorts pins
      let filteredPins =
      pins
        .filter(pin => filterPin(filter, pin))
        .sort(comparePin)

      // Enforce limit
      if(filteredPins.length > limit) {
        filteredPins = filteredPins.slice(0, limit)
      }

      res.status(200).json({
        count: filteredPins.length,
        results: filteredPins
      } as PinsResult)
    })
    .catch(err => next(err))
})


// Add pin
PinsController.post('/', (req, res, next) => {
  // TODO validate input
  service.add(req.user, req.body as Pin)
    .then(createdPin => res.status(201).json(createdPin as PinStatus))
    .catch(err => next(err))
})


// Get status
PinsController.get('/:requestid', (req, res, next) => {
  if(!req.params.requestid) {
    throw new BadRequestException('Missing requestid')
  }

  service.status(req.user, req.params.requestid)
    .then(pinStatus => res.status(200).json(pinStatus as PinStatus))
    .catch(err => next(err))
})


// Update pin
PinsController.post('/:requestid', (req, res) => {
  throw new NotYetImplementedException(`Update pin ${req.params.requestid}`)
  return res.status(200).json({} as PinStatus)
})


// Delete pin
PinsController.delete('/:requestid', (req, res, next) => {
  if(!req.params.requestid) {
    throw new BadRequestException('Missing requestid')
  }

  service.remove(req.user, req.params.requestid)
    .then(() => res.status(200).json())
    .catch(err => next(err))
})


export { PinsController }
