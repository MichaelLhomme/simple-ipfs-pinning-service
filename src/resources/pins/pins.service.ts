import { v4 as uuidv4 } from 'uuid'

import { NotFoundException } from '~/utils/exceptions'
import { Pin, PinStatus, Status } from '~~/types/pins'

import {
  CID,
  getDelegates,

  fileExists,
  writeFile,
  readFile,
  listFiles,
  deleteFile,

  pinAdd,
  pinRm,
  pinStatus,

  createSimpleFile,
  createSimpleDirectory
} from '~/utils/ipfs'


/*
 * Local interface describing the format used to store
 * pin requests in MFS
 */
interface PinStorage {
  pinStatus: PinStatus
  dag: string
}


/**
 * PinsService
 */
export class PinsService {

  //list pîns
  async list(user: string): Promise<PinStatus[]> {
    const requests =
      (await listFiles(user))
        .map(async (filename: string) => {
          const json: PinStorage = await readFile(`${user}/${filename}`)
          json.pinStatus.status = await pinStatus(json.pinStatus.pin.cid)
          return json.pinStatus
        })

    return await Promise.all(requests)
  }


  // add pin
  async add(user: string, pin: Pin): Promise<PinStatus> {
    // Create pinStatus object
    const pinStatus = {
      requestid: uuidv4(),
      created: new Date().toJSON(),
      status: Status.Queued,
      pin
    }

    // Create the DAG for pinning
    const dagCID = await this.createStorageDAG(pinStatus)

    // Write the request to MFS
    await writeFile(`${user}/${pinStatus.requestid}`, {
      pinStatus,
      dag: dagCID.toString()
    })

    // Add a pin to the storage DAG
    await pinAdd(dagCID)

    return {
      ...pinStatus,
      delegates: await getDelegates()
    }
  }


  // remove pin
  async remove(user: string, requestid: string): Promise<void> {
    // Read the request
    const json: PinStorage = await readFile(`${user}/${requestid}`)

    // Remove pin only if needed
    const status = await pinStatus(json.pinStatus.pin.cid)
    if(status === Status.Pinned || status === Status.Pinning) {
      await pinRm(json.dag)
    }

    // Remove the request
    await deleteFile(`${user}/${requestid}`)
  }


  // get pin status
  async status(user: string, requestid: string): Promise<PinStatus> {
    if(!await fileExists(`${user}/${requestid}`)) {
      throw new NotFoundException(`Request id not found: ${requestid}`)
    }

    // Read record and update status
    const json: PinStorage = await readFile(`${user}/${requestid}`)
    json.pinStatus.status = await pinStatus(json.pinStatus.pin.cid)
    return json.pinStatus
  }


  // Internal helper to create the DAG for content pinning
  private async createStorageDAG(pinStatus: PinStatus): Promise<string> {
    const contentCID = CID.parse(pinStatus.pin.cid)
    const fileCID = await createSimpleFile(JSON.stringify(pinStatus))
    const dirCID = await createSimpleDirectory([
      { name: 'pinStatus.json', cid: fileCID },
      { name: 'content', cid: contentCID }
    ])

    return dirCID.toString()
  }

}
