import cors from 'cors'
import express from 'express'

import { config } from '~/config'

import AuthenticationHandler from '~/middlewares/authentication.handler'
import { ExceptionsHandler } from '~/middlewares/exceptions.handler'
import { UnknownRoutesHandler } from '~/middlewares/unknownRoutes.handler'

import { PinsController } from '~/resources/pins/pins.controller'

// Start express
const app = express()
app.use(express.json())
app.use(cors())

// Setup API
app.use(AuthenticationHandler)
app.use('/pins', PinsController)
app.all('*', UnknownRoutesHandler)
app.use(ExceptionsHandler)

// Start server
app.listen(config.API_PORT, () => console.log(`IPFS Pinning Service listening on port ${config.API_PORT}`))
