import { NextFunction, Request, Response } from 'express'
import { Exception } from '~/utils/exceptions'

export const ExceptionsHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  if (res.headersSent) {
    return next(err)
  }

  if (err instanceof Exception) {
    return res
      .status(err.status)
      .json(err.toJSON())
  }

  return res
    .status(500)
    .json({
      error: {
        reason: 'INTERNAL_SERVER_ERROR',
        details: `Unknown internal error: ${err}`
      }
    })
}
