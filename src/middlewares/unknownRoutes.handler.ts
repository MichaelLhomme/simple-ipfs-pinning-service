import { NotFoundException } from '~/utils/exceptions'

// Default handler generates NotFoundException
export const UnknownRoutesHandler = () => {
  throw new NotFoundException('The requested resource doesn\'t exists')
}
