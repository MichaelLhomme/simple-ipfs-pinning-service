import { NextFunction, Request, Response } from 'express'

import { accountExists } from '~/utils/ipfs'
import { BadRequestException, NotAuthorizedException } from '~/utils/exceptions'


/*
 * Authenticate request using MFS to check the existence of an account
 */
function authenticate(req: Request, res: Response, next: NextFunction) {
  if(!req.headers || !req.headers.authorization) {
    throw new BadRequestException('Missing authorization header')
  }

  const [_, token] = req.headers.authorization.split(' ')
  if(!token) {
    throw new BadRequestException('Malformed authorization header')
  }

  accountExists(token)
    .then(exists => {
      if(!exists) {
        next(new NotAuthorizedException())
      }

      req.user = token
      next()
    })
}


export default authenticate
