import { ApiException } from '~~/types/exceptions'


export class Exception {
  status: number
  reason: string
  details: string

  constructor(status: number, reason: string, details: string) {
    this.status = status
    this.reason = reason
    this.details = details
  }

  toJSON(): ApiException {
    return {
      error: {
        reason: this.reason,
        details: this.details
      }
    }
  }
}


export class NotFoundException extends Exception {
  constructor(details: string) {
    super(404, 'NOT_FOUND', details)
  }
}


export class BadRequestException extends Exception {
  constructor(details: string) {
    super(400, 'BAD_REQUEST', details)
  }
}


export class NotAuthorizedException extends Exception {
  constructor() {
    super(401, 'UNAUTHORIZED', 'Access token is invalid')
  }
}


export class NotYetImplementedException extends Exception {
  constructor(details?: string) {
    super(500, 'NOT YET IMPLEMENTED', details || 'This API endpoint is not implemented')
  }
}
