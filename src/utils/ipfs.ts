import { UnixFS } from 'ipfs-unixfs'
import HTTP from 'ipfs-utils/src/http.js'
import { create, CID } from 'ipfs-http-client'
import { createLink, createNode } from '@ipld/dag-pb'

import { concat as uint8ArrayConcat } from 'uint8arrays/concat'
import { toString as uint8ArrayToString } from 'uint8arrays/to-string'
import { fromString as uint8ArrayFromString } from 'uint8arrays/from-string'

import { Status } from '~~/types/pins'

// Re-export here for convenience
export { CID }

const LOCAL_URL = 'http://127.0.0.1:5001/api/v0'
const ROOT_PATH = '/.simple-pinning-service'


/**
 * General
 */

// Create global client instance
export const client = create({ url: LOCAL_URL })


// List known addresses of the node
export async function getDelegates(): Promise<string[]> {
  // TODO filter localhost (local addresses may be useful on a lan)
  const id = await client.id()
  return id.addresses.map(addr => addr.toString())
}


/**
 * MFS primitives
 */

function relativePath(path: string) {
  return `${ROOT_PATH}/${path}`
}


export async function fileExists(filename: string): Promise<boolean> {
  try {
    await client.files.stat(relativePath(filename))
    return true
  } catch(err) {
    return false
  }
}


export async function accountExists(token: string): Promise<boolean> {
  return fileExists(token)
}


export async function writeFile(filename: string, content: any): Promise<void> {
  try {
    await client.files.write(relativePath(filename), JSON.stringify(content), { create: true })
  } catch(err) {
    console.error(`Error writing to file ${filename}`, err)
    throw new Error(`Error writing file`)
  }
}


export async function deleteFile(filename: string): Promise<void> {
  try {
    await client.files.rm(relativePath(filename))
  } catch(err) {
    console.error(`Error removing file ${filename}`, err)
    throw new Error(`Error removing file`)
  }
}


export async function readFile<T>(filename: string): Promise<T> {
  try {
    const chunks = []
    for await(const chunk of await client.files.read(relativePath(filename))) {
      chunks.push(chunk)
    }
    const str = uint8ArrayToString(uint8ArrayConcat(chunks))
    return JSON.parse(str) as T
  } catch(err) {
    console.error(`Error reading file ${filename}`, err)
    throw new Error(`Error reading file`)
  }
}


export async function listFiles(base: string): Promise<string[]> {
  try {
    const files = []
    for await (const file of client.files.ls(relativePath(base))) {
      files.push(file)
    }
    return files.map(mfs => mfs.name)
  } catch(err) {
    console.error(`Error listing path ${base}`, err)
    throw new Error(`Error listing path`)
  }
}


/**
 * Pins helpers
 */

export async function pinAdd(cid: string): Promise<void> {
  client.pin.add(cid)
}


export async function pinRm(cid: string): Promise<void> {
  client.pin.rm(cid)
}


export async function pinStatus(cid: string): Promise<Status> {
  try {
    const search = client.pin.ls({ paths: CID.parse(cid) })

    // only look for the last record (should only be 1 anyway)
    let results = ''
    for await(const { type } of search) {
      results = type
    }

    if(results === 'recursive' || results.startsWith('indirect')) {
      return Status.Pinned
    } else {
      return Status.Pinning
    }
  } catch(err) {
    // Prevent errors when content is not yet pinned

    // TODO instanceof is not working ?
    // if(err instanceof HTTP.HTTPError) {
    //   return Status.Queued
    // }
    if(err && (err as Record<string, string>)?.name === 'HTTPError') {
      return Status.Queued
    }

    console.error(`Error reading pin status for CID ${cid}`, err)
    throw new Error(`Error reading pin status`)
  }
}


/**
 * DAG helpers
 */

export async function createSimpleFile(content: string): Promise<CID> {
  const file = new UnixFS({
    type: 'file',
    data: uint8ArrayFromString(content)
  })
  const fileValue = createNode(file.marshal())
  return await client.dag.put(fileValue, { storeCodec: 'dag-pb' })
}


export async function createSimpleDirectory(links: { cid: CID, name: string }[]): Promise<CID> {
  const dir = new UnixFS({ type: 'directory' })
  const dirValue = createNode(dir.marshal(), links.map(link => createLink(link.name, 100, link.cid)))
  return await client.dag.put(dirValue, { storeCodec: 'dag-pb' })
}
