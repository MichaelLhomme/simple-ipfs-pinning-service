/* Web API types */

type CID = string
type MultiAddr = string
type Timestamp = string
type Meta = Record<string, any>
type Info = Record<string, any>


export enum Status {
  Queued = 'queued',
  Pinning = 'pinning',
  Pinned = 'pinned',
  Failed = 'failed'
}


export interface Pin {
  cid: CID
  name: string
  origins?: MultiAddr[]
  meta?: Meta
}


export interface PinStatus {
  requestid: string
  status: Status
  created: Timestamp
  pin: Pin
  delegates?: MultiAddr[]
  info?: Info
}


export interface PinFilter {
  cid?: CID
  name?: string
  match?: 'exact' | 'iexact' | 'partial' | 'ipartial'
  status?: string
  before?: Timestamp
  after?: Timestamp
  meta?: Record<string, any>
  limit?: number
}


export interface PinsResult {
  count: number,
  results: PinStatus[]
}
