export interface ApiException {
  error: {
    reason: string,
    details: string
  }
}
