// Import is required otherwise the new field isn't recognized by express
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Express } from 'express'

declare global {
    namespace Express {
        interface Request {
            user: string
        }
    }
}
