# Simple IPFS Pinning Service

NodeJS implementation of the [IPFS Pinning Service API](https://ipfs.github.io/pinning-services-api-spec/), relying on a local IPFS node for data management

- Data is pinned on a local node
- Metadata are stored on a local node using MFS
- API key is created manually on MFS

## TODO

- implement pin update
- add support for IPNS using metadata
- autorefresh of IPNS content

## Known problems

### Dependency `ipfs-http-client` is locked

Locked on version 56 due to compilation errors with 57

```
Error [ERR_PACKAGE_PATH_NOT_EXPORTED]: No "exports" main defined in node_modules/ipfs-http-client/package.json
```
